Lab6 - connect to external database (HSQLdb/MySQL)
          modify application.properties
               spring.jpa.hibernate.ddl-auto=create
               spring.jpa.show-sql=true
               spring.datasource.url=jdbc:mysql://host:3306/mydb
               spring.datasource.username=admin
               spring.datasource.password=MyPassword
Lab5 - Spring data 
     create new maven proj with spring Initializer
          Day4.demo4
          Dependencies - data jpa, hsqldb, mysql driver
               comment mysql driver
     Create demo.Dept 
          package demo;
          import javax.persistence.Entity;
          import javax.persistence.Id;
          import javax.persistence.Table;

          @Entity
          @Table(name = "depttable")
          public class Dept {
               @Id
               private int deptno;
               private String dname;
               private String loc;     
               + get/set + tostring + 2 constructors
     create demo.DeptRepo
          package demo;

          import org.springframework.data.jpa.repository.JpaRepository;
          import org.springframework.stereotype.Repository;
          @Repository
          public interface  DeptRepo extends JpaRepository<Dept, Integer> {
          
          }
     create demo.application.java
          package demo;

          import org.springframework.beans.factory.annotation.Autowired;
          import org.springframework.boot.SpringApplication;
          import org.springframework.boot.autoconfigure.SpringBootApplication;
          import org.springframework.context.ApplicationContext;
          import org.springframework.context.annotation.Bean;

          @SpringBootApplication
          public class Application {

               @Autowired
               private DeptRepo repo;
               @Bean
               public String init() {
                    for (int i = 10;i<100;i+=10) {
                         Dept dept =new Dept(i,"DNAMEof"+i,"Pnq");
                         if ( (i %20)==0)
                              dept.setLoc("Hyd");
                                   repo.save(dept);
                    }
                    repo.deleteById(20);
                    
                    Dept dept =new Dept(50,"Finance","Blr");
                    repo.save(dept);
                    repo.findAll().forEach(d->System.out.println(d));
                    return "s";
               }
               public static void main(String[] args) {
                    ApplicationContext ctx = SpringApplication.run(Application.class, args);
                    }
          }
     create application.properties in src/main/resources
          spring.jpa.hibernate.ddl-auto=create
          spring.jpa.show-sql=true
     run 

Lab4 - modify passwordencoder 
     add following method MySecurityConfig
          @Bean
          public PasswordEncoder passEncoder() {
               return new BCryptPasswordEncoder(); 
          }
     change userDetailsService
     	@Bean
          public UserDetailsService userDetailsService() {
               PasswordEncoder encoder = passEncoder(); 
               String hashedpass = encoder.encode("password");
               System.out.println("HashedPassword " + hashedpass);
               UserDetails user =
                    User.withUsername("user")
                         .password(hashedpass)
                         .roles("USER")
                         .build();

               return new InMemoryUserDetailsManager(user);
          }
Lab3 - Spring Security
     Spring Initializer - Day4.demo3 
          web
     FirstController -> page with two hlinks (admin/m1, admin/m2)
          admin/m1 -> m1 invoked
          admin/m2 -> m2 invoked.
          package demo;

          import org.springframework.web.bind.annotation.GetMapping;
          import org.springframework.web.bind.annotation.RequestMapping;
          import org.springframework.web.bind.annotation.RestController;

          @RestController(value = "/")
          public class FirstController {
               @GetMapping
               public String index() {
                    String s1= "<html><body><h1>Index Page</h1>" + 
                                        "<h1><a href='admin/m1'>Admin method1 </a></h1>" + 
                                        "<h1><a href='admin/m2'>Admin method2 </a></h1></body></html>";
                    return s1;
               }
               
               @GetMapping(value = "admin/m1")
               public String method1()
               {
                    return "<h1>Method1</h1><h2><a href='/'>Index Page</a></h2>";
               }
               @GetMapping(value = "admin/m2")
               public String method2()
               {
                    return "<h1>Method2</h1><h2><a href='/'>Index Page</a></h2>";
               }
          }
     Modify pom file to include 
     	<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
     Run ->    spring security password is generated
               index page is also not shown, just direct login page 
               no valid user
     Create demo.MySecurityConfig.java (https://spring.io/guides/gs/securing-web/)
          package demo;
          @Configuration
          @EnableWebSecurity
          public class MySecurityConfig {

               @Bean
               public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
                    http
                         .authorizeHttpRequests((requests) -> requests
                              .antMatchers("/", "/home").permitAll()
                              .anyRequest().authenticated()
                         )
                         .formLogin();
                    return http.build();
               }

               @Bean
               public UserDetailsService userDetailsService() {
                    UserDetails user =
                         User.withDefaultPasswordEncoder()
                              .username("user")
                              .password("password")
                              .roles("USER")
                              .build();

                    return new InMemoryUserDetailsManager(user);
               }
          }
     Run -> http://localhost:8080/ 
                    index page is shown without auth.
                    on admin/m1 -> should ask for details
                    once logged in -> can invoke m1, m2 without logging in again
                // new incognito mode -> close browser     


Lab2 -  Spring Boot web
     Spring Initializer
          Maven, Java, 2.7.17,
     Day4.demo2
     package - demo
     packaging - jar   
     java - 8
     Add Dependency - web
     Generate, 
     copy downloaded jar(extracted) in project workspace, import as Maven
     create simple FirstController in demo package
     run the application and check
          http://localhost:8080/first


Lab1 - Spring Boot 
     Spring Initializer
          Maven, Java, 2.7.17,

     Day4.demo1
     package - demo
     packaging - jar   
     java - 8
     No Dependency
     Generate, 
     copy downloaded jar(extracted) in project workspace, import as Maven
     run the application and check

     
