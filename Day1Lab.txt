Lab7  -  ORM
        Day1.demo4
        POM file to include 
            <!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>5.3.30</version>
            </dependency>
            <!-- https://mvnrepository.com/artifact/org.springframework/spring-orm -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-orm</artifactId>
                <version>5.3.30</version>
            </dependency>
            <!-- https://mvnrepository.com/artifact/org.hibernate/hibernate-entitymanager -->
            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-entitymanager</artifactId>
                <version>5.6.15.Final</version>
            </dependency>
        create demo.DeptDAO
            package demo;
            import java.util.List;

            import javax.annotation.PostConstruct;

            import org.hibernate.SessionFactory;
            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.orm.hibernate5.HibernateTemplate;
            import org.springframework.stereotype.Component;
            import org.springframework.transaction.annotation.Transactional;

            @Component(value="deptdao")
            @Transactional
            public class DeptDAO {
                @Autowired
                private SessionFactory sf;
                HibernateTemplate template;
                
                @PostConstruct
                public void init() {
                    template = new HibernateTemplate(sf);
                }
                public void insert(Dept d) {
                    template.persist(d);
                }
                
                public List<Dept> list(){
                    return template.loadAll(Dept.class);
                }
                
                public void delete(int deptno) {
                    Dept d= template.get(Dept.class, deptno);
                    template.delete(d);
                }
                public void update (Dept moddept)
                {
                    template.update(moddept);
                }
            }


        create demo.Dept
            package demo;
            import javax.persistence.Entity;
            import javax.persistence.Id;
            import javax.persistence.Table;

            @Entity
            @Table(name = "depttable")
            public class Dept {
                @Id
                private int deptno;
                private String dname;
                private String loc;
                public int getDeptno() {
                    return deptno;
                }
                public void setDeptno(int deptno) {
                    this.deptno = deptno;
                }
                public String getDname() {
                    return dname;
                }
                public void setDname(String dname) {
                    this.dname = dname;
                }
                public String getLoc() {
                    return loc;
                }
                public void setLoc(String loc) {
                    this.loc = loc;
                }
                @Override
                public String toString() {
                    return "Dept [deptno=" + deptno + ", dname=" + dname + ", loc=" + loc + "]";
                }
                public Dept(int deptno, String dname, String loc) {
                    this.deptno = deptno;
                    this.dname = dname;
                    this.loc = loc;
                }
                public Dept() {
                }
            }
        create MyConfig
        package demo;
            import java.util.Properties;

            import javax.sql.DataSource;

            import org.springframework.context.annotation.Bean;
            import org.springframework.context.annotation.Configuration;
            import org.springframework.jdbc.datasource.DriverManagerDataSource;
            import org.springframework.orm.hibernate5.HibernateTransactionManager;
            import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
            import org.springframework.transaction.PlatformTransactionManager;
            import org.springframework.transaction.annotation.EnableTransactionManagement;

            @Configuration
            @EnableTransactionManagement
            public class MyConfig {

                // database
                public DataSource datasource() {
                    DriverManagerDataSource ds = new DriverManagerDataSource();
                    ds.setDriverClassName("org.hsqldb.jdbc.JDBCDriver");
                    ds.setUrl("jdbc:hsqldb:hsql://localhost/");
                    ds.setUsername("SA");
                    ds.setPassword("");
                    return ds;
                }
                
                @Bean
                    public PlatformTransactionManager hibernateTransactionManager() {
                        HibernateTransactionManager transactionManager
                        = new HibernateTransactionManager();
                        transactionManager.setSessionFactory(sessionFactory().getObject());
                        return transactionManager;
                    }
                
            //sessionFactory
                @Bean
                public LocalSessionFactoryBean sessionFactory() {
                    LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
                    sessionFactory.setDataSource(datasource());
                    sessionFactory.setPackagesToScan("demo");
                    sessionFactory.setHibernateProperties(hibernateProperties());

                    return sessionFactory;
                }


                private final Properties hibernateProperties() {
                    Properties hibernateProperties = new Properties();
                    hibernateProperties.setProperty(
                    "hibernate.hbm2ddl.auto", "create-drop");
                    hibernateProperties.setProperty(
                    "hibernate.dialect", "org.hibernate.dialect.HSQLDialect");

                    return hibernateProperties;
                }
                // transactions 
            }
        Write client to test 
            package demo;

            import org.springframework.context.ApplicationContext;
            import org.springframework.context.annotation.AnnotationConfigApplicationContext;
            import org.springframework.context.annotation.ComponentScan;

            @ComponentScan(basePackages = "demo")
            public class Client {

              	public static void main(String[] args) {
                    // TODO Auto-generated method stub
                        ApplicationContext ctx = new AnnotationConfigApplicationContext(Client.class);
                        DeptDAO dao = ctx.getBean("deptdao", DeptDAO.class);
                        
                        for (int i = 10;i<100;i+=10) {
                            Dept dept =new Dept(i,"DNAMEof"+i,"Pnq");
                            if ( (i %20)==0)
                                dept.setLoc("Hyd");
                                    dao.insert(dept);
                        }
                        
                        dao.delete(20);
                        Dept dept =new Dept(50,"Finance","Blr");
                        dao.update(dept);
                        dao.list().forEach(d->System.out.println(d));
                        }

            }    


Lab6 - in same project
        create interface Connection with open and close methods
        create two implementation classes OracleConnection, SQLConnection
        create a class DeptDAO with save method and a variable for Connection
     
            package demo;

            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.beans.factory.annotation.Qualifier;
            import org.springframework.stereotype.Component;

            @Component(value="deptdao")
            public class DeptDAO  
            {
                @Autowired
                @Qualifier(value = "mysql")
                private Connection con;
                
                public void insert() {
                    getCon().open();
                    System.out.println("in insert method of DeptDAO");
                    getCon().close();
                }

                public Connection getCon() {
                    return con;
                }

                public void setCon(Connection con) {
                    this.con = con;
                }
            }

Lab5 - in same project  
        Modify First class to replace default constructor to parameterized constructor
        run -> watch error
        delete all annoations from First.java
        Create a file MyConfig
            package demo;

            import org.springframework.context.annotation.Bean;
            import org.springframework.context.annotation.Configuration;

            @Configuration
            public class MyConfig {
                @Bean
                public First first() {
                    System.out.println("first method invoked..");
                    return new First("Vaishali");
                }
            }

        run 
        use @scope and @lazy for first method        
Lab4 - 
    new maven project -Day1.demo3
        delete demo.xml -> observe error
        Modify Client.java to 
            package demo;

            @ComponentScan(basePackages = "demo")
            public class Client {

                public static void main(String[] args) {
                    ApplicationContext ctx = new AnnotationConfigApplicationContext(Client.class);
                    System.out.println("----------------After context is created ---------------------");
                    Simple s1 = ctx.getBean("sim", Simple.class);
                    s1.m1();
                    Simple s2 = ctx.getBean("sim", Simple.class);
                    s2.m1();
                    Simple s3 = ctx.getBean("sim", Simple.class);
                    s3.m1();
                    First f1 = ctx.getBean("first", First.class);
                    First f2 = ctx.getBean("first", First.class);
                    First f3 = ctx.getBean("first", First.class);
                }
            }
        Observe error for getBean first
        Create demo.First with @component, constructor and a method
        Add @Lazy annotation to First at class level

Lab3 - create new maven project - simple    
        Day1.demo2
        Modify pom.xml to include 
            <!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>5.3.30</version>
            </dependency>
        Create demo.xml in resourses
            <?xml version="1.0" encoding="UTF-8"?>
            <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:context="http://www.springframework.org/schema/context"
                xsi:schemaLocation="http://www.springframework.org/schema/beans
                    https://www.springframework.org/schema/beans/spring-beans.xsd
                    http://www.springframework.org/schema/context
                    https://www.springframework.org/schema/context/spring-context.xsd">
                    <context:component-scan base-package="demo"></context:component-scan>	
            </beans>
        create Simple.java
            package demo;

            public class Simple {
                public Simple() {
                    System.out.println("in Simple Constructor");
                }

                public void m1() {
                    System.out.println("m1 from Simple");
                }
            }
        create Client.java
                package demo;
                import org.springframework.context.ApplicationContext;
                import org.springframework.context.support.ClassPathXmlApplicationContext;
                public class Client {

                    public static void main(String[] args) {
                        // Simple s1 = new Simple();
                        ApplicationContext ctx = new ClassPathXmlApplicationContext("demo.xml");
                        System.out.println("----------------After context is created ---------------------");
                        Simple s1 = ctx.getBean("sim", Simple.class);
                        s1.m1();
                        Simple s2 = ctx.getBean("sim", Simple.class);
                        s2.m1();
                        Simple s3 = ctx.getBean("sim", Simple.class);
                        s3.m1();
                    }
                }
            run -> check error
            Modify simple class to add @Component(value = "sim") annotation on class level
            run -> observe singleton
            add @Scope(value = "prototype") and observe change of constructor calls

            Create a file Simple1 -> with constructor, a function using annotations
                modify client to create instance and invoke method.


Lab2 - eclispe -> create new maven project - simple
        Group+artifact = Day1.demo1

        Modify pom.xml to include 
            <!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>5.3.30</version>
            </dependency>
        Create demo.xml in resourses
            <?xml version="1.0" encoding="UTF-8"?>
            <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="
                    http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

                <!-- bean definitions here -->
                <bean id="sim" class="demo.Simple"/>
            </beans>
        create Simple.java

            package demo;

            public class Simple {
                public Simple() {
                    System.out.println("in Simple Constructor");
                }

                public void m1() {
                    System.out.println("m1 from Simple");
                }
            }
        create Client.java
            package demo;

            import org.springframework.context.ApplicationContext;
            import org.springframework.context.support.ClassPathXmlApplicationContext;

            public class Client {

                public static void main(String[] args) {
                    // Simple s1 = new Simple();
                    ApplicationContext ctx = new ClassPathXmlApplicationContext("demo.xml");
                    System.out.println("----------------After context is created ---------------------");
                    Simple s1 = ctx.getBean("sim", Simple.class);
                    s1.m1();
                    Simple s2 = ctx.getBean("sim", Simple.class);
                    s2.m1();
                }

            }
        run -> observe only single instance of  Simple.java is created
        Modify xml file to add scope parameter to bean tag
                scope="prototype"
                run and check output

Lab1 - https://www.cesarsotovalero.net/blog/inversion-of-control-and-dependency-injection-in-java.html#:~:text=Inversion%20of%20Control%20(IoC)%20is,that%20applies%20the%20IoC%20principle.
